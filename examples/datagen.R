D = list()
n = 10

#### aces ####
set.seed(7124)
D$aces = tibble::tibble(id = 1:n)
for(i in 1:23)
  D$aces[[paste0("aces",i)]] = sample(c(0,1),n,replace = T)
D$aces$aces24 = sample(0:5, n, replace = T)

#### ASC ####
set.seed(2177)
D$asc = tibble::tibble(id = 1:n)
for(i in 1:12)
  D$asc[[paste0("asc",i)]] = sample(0:2,n,replace = T)

#### BDI ####
set.seed(4122)
D$bdi = tibble::tibble(id = 1:n)
for(i in 1:21)
  D$bdi[[paste0("bdi",i)]] = sample(0:3,n,replace = T)
D$bdi$bdi19_eatless = sample(0:1, n, replace = T)

#### bisbas ####
set.seed(9237)
D$bisbas = tibble::tibble(id = 1:n)
for(i in 1:24)
  D$bisbas[[paste0("bisbas",i)]] = sample(1:4,n,replace = T)

#### bpi ####
set.seed(8583)
D$bpi = tibble::tibble(id = 1:n)
for(i in 1:4)
  D$bpi[[paste0("bpi",i)]] = sample(0:10,n,replace = T)
for(i in letters[1:7])
  D$bpi[[paste0("bpi5",i)]] = sample(0:10,n,replace = T)

#### Barrier BQ13 ####
set.seed(9770)
D$bq13 = tibble::tibble(id = 1:n)
for(i in 1:13)
  D$bq13[[paste0("bq13_",i)]] = sample(0:5,n,replace = T)


#### Brunel ####
set.seed(6270)
D$brunel = tibble::tibble(id = 1:n)
for(i in 1:9)
  D$brunel[[paste0("brunel",i)]] = sample(1:5,n,replace = T)

#### CESD ####
set.seed(6408)
D$cesd = tibble::tibble(id = 1:n)
for(i in 1:20)
  D$cesd[[paste0("cesd",i)]] = sample(0:3,n,replace = T)

#### ERS ####
set.seed(9948)
D$ers = tibble::tibble(id = 1:n)
for(i in 1:14)
  D$ers[[paste0("ers",i)]] = sample(1:4,n,replace = T)

#### FFMQ ####
set.seed(6506)
D$ffmq = tibble::tibble(id = 1:n)
for(i in 1:39)
  D$ffmq[[paste0("ffmq",i)]] = sample(1:5,n,replace = T)

#### FoP ####
set.seed(8756)
D$fop = tibble::tibble(id = 1:n)
for(i in 1:30)
  D$fop[[paste0("fop",i)]] = sample(1:5,n,replace = T)

#### GAD7 ####
set.seed(6835)
D$gad7 = tibble::tibble(id = 1:n)
for(i in 1:7)
  D$gad7[[paste0("gad7_",i)]] = sample(0:3,n,replace = T)

#### HIT6 ####
set.seed(5438)
D$hit6 = tibble::tibble(id = 1:n)
for(i in 1:6)
  D$hit6[[paste0("hit6_",i)]] = sample(c(6,8,10,11,13),n,replace = T)

#### ISI ####
set.seed(218)
D$isi = tibble::tibble(id = 1:n)
for(i in letters[1:3])
  D$isi[[paste0("isi1",i)]] = sample(0:4,n,replace = T)
for(i in 2:5)
  D$isi[[paste0("isi",i)]] = sample(0:4,n,replace = T)

#### Morningness ####
set.seed(8322)
D$morningness = tibble::tibble(id = 1:n)
for(i in c(1,2,10,17,18))
  D$morningness[[paste0("morningness",i)]] = sample(1:5,n,replace = T)
for(i in c(3,4,5,6,7,8,9, 11,12,13,14,15, 16))
  D$morningness[[paste0("morningness",i)]] = sample(1:4,n,replace = T)
D$morningness$morningness19 = sample(c(0,2,4,6), n, replace = T)

D$morningness = dplyr::select(D$morningness,
                              id,
                              morningness1,
                              morningness2,
                              morningness3,
                              morningness4,
                              morningness5,
                              morningness6,
                              morningness7,
                              morningness8,
                              morningness9,
                              morningness10,
                              morningness11,
                              morningness12,
                              morningness13,
                              morningness14,
                              morningness15,
                              morningness16,
                              morningness17,
                              morningness18,
                              morningness19)

#### pain sensitivity ####
set.seed(5703)
D$painsensitivity = tibble::tibble(id = 1:n)
for(i in 1:17)
  D$painsensitivity[[paste0("painsensitivity",i)]] = sample(0:10,n,replace = T)

#### panas ####
set.seed(2081)
D$panas = tibble::tibble(id = 1:n)
for(i in 1:60)
  D$panas[[paste0("panas",i)]] = sample(1:5,n,replace = T)

#### pcs ####
set.seed(2684)
D$pcs = tibble::tibble(id = 1:n)
for(i in 1:13)
  D$pcs[[paste0("pcs",i)]] = sample(0:4,n,replace = T)

#### PHQ 15 ####
set.seed(6827)
D$phq15 = tibble::tibble(id = 1:n)
for(i in 1:15)
  D$phq15[[paste0("phq15_",i)]] = sample(0:2,n,replace = T)

#### PHQ 9 ####
set.seed(733)
D$phq9 = tibble::tibble(id = 1:n)
for(i in 1:9)
  D$phq9[[paste0("phq9_",i)]] = sample(0:3,n,replace = T)

#### Pill ####
set.seed(8481)
D$pill = tibble::tibble(id = 1:n)
for(i in 1:54)
  D$pill[[paste0("pill",i)]] = sample(1:5,n,replace = T)

#### PSAS ####
set.seed(7780)
D$psas = tibble::tibble(id = 1:n)
for(i in 1:16)
  D$psas[[paste0("psas",i)]] = sample(1:5,n,replace = T)

#### PSQ  ####
set.seed(1787)
D$psq = tibble::tibble(id = 1:n)
for(i in 1:18)
  D$psq[[paste0("painsensitivity",i)]] = sample(0:10,n,replace = T)

#### PSQI ####

set.seed(9665)
D$psqi = tibble::tibble(id = 1:n)
D$psqi$psqi1 = paste0(sample(c("22","23","00","01","02"), n, replace = T),":",sample(c("00","15","30","45"),n,replace = T))
D$psqi$psqi2 = sample(0:120,n,replace = T)
D$psqi$psqi3 = paste0(sample(c("04","05","06","07","08","09","10"), n, replace = T),":",sample(c("00","15","30","45"),n,replace = T))
D$psqi$psqi4 = sample(5:12,n,replace = T)

for(i in letters[1:10])
  D$psqi[[paste0("psqi5",i)]] = sample(0:3,n,replace = T)
for(i in 6:9)
  D$psqi[[paste0("psqi",i)]] = sample(0:3,n,replace = T)

#### PSS10 ####
set.seed(3168)
D$pss = tibble::tibble(id = 1:n)
for(i in 1:10)
  D$pss[[paste0("pss",i)]] = sample(0:4,n,replace = T)

#### SCQ ####
set.seed(5104)
D$scq = tibble::tibble(id = 1:n)
for(i in 1:6)
  D$scq[[paste0("scq",i)]] = sample(0:4,n,replace = T)

#### SF MPQ 2####
set.seed(7856)
D$sfmpq2 = tibble::tibble(id = 1:n)
for(i in 1:22)
  D$sfmpq2[[paste0("sfmpq2_",i)]] = sample(0:10,n,replace = T)

#### TEPS ####
set.seed(57439)
D$teps = tibble::tibble(id = 1:n)
for(i in 1:18)
  D$teps[[paste0("teps",i)]] = sample(1:6,n,replace = T)

#### TMMS ####
set.seed(8887)
D$tmms = tibble::tibble(id = 1:n)
for(i in 1:30)
  D$tmms[[paste0("tmms",i)]] = sample(1:5,n,replace = T)

#### write examples ####
for(tablename in names(D)){
  DD = D[[tablename]]
  write.csv(DD,paste0("i:/jmtremblay/batesmo/inst/extdata/",tablename,".csv"),row.names = F, quote = F)
}
